<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller{
	//class constructor
	function __construct(){
		parent::__construct();

    $this->load->helpers('my_helper');//function to avoid back button to dashboard after login out
    no_cache();

    //load the default header page
    //side menu statistics
    $side['blc']=$this->pro_model->blockedusers();
    $side['new']=$this->pro_model->statnewusers();
    $side['mle']=$this->pro_model->malestudents();
    $side['fmle']=$this->pro_model->femalestudents();
    $side['tch']=$this->pro_model->teachingstaff();
    
    $this->load->view("headertop",$side);
   
	} 
  //the admins dashboard
	public function index(){
    //stat boxes data
    $stats['pending']=$this->pro_model->statnewusers();
    $stats['male']=$this->pro_model->malestudents();
    $stats['female']=$this->pro_model->femalestudents();
    $stats['teacher']=$this->pro_model->teachingstaff();

		$this->load->view("admin_dashboard",$stats);
	}

 

	//the login function
	public function login() //login
    {
      //user inputs validation
      $login_user=array(
        array(
          'field'=>'username',
          'label'=>'The Username',
          'rules'=>'trim|required|xss_clean'
          ),
        array(
          'field'=>'password',
          'label'=>'The Password',
          'rules'=>'trim|required|xss_clean'
          )
        );
      $this->form_validation->set_rules($login_user);

      if($this->form_validation->run() === FALSE)
      {
        $this->load->view('login'); //redirects back to show errors
      }

      else
      { 
      //calls the model to login and checks user type

        //type school admin
       if($this->input->post('type')=="School Admin"){
        $this->pro_model->admin_login();
      
        if($this->pro_model->admin_login() === FALSE)
        {
          $this->session->set_flashdata('msg', 'Wrong Username or Password!');
           redirect('login');
        }

        else
        {

          redirect('dashboard');
          
        }
       }
         

         //type system admin
       if($this->input->post('type')=="System Admin"){
        $this->pro_model->systemadmin_login();
      
        if($this->pro_model->systemadmin_login() === FALSE)
        {
          $this->session->set_flashdata('msg', 'Wrong Username or Password!');
           redirect('login');
        }

        else
        {

          redirect('superadmin');

          
        }
       }
      }
    }
    
    //admin logout
    function logout() 
   {
      $this->session->sess_destroy();

      redirect('login');
    }

    //new users page
    function newusers(){

      //new users table data
      $new['pending']=$this->pro_model->statnewusers();
      $new['male']=$this->pro_model->malestudents();
      $new['female']=$this->pro_model->femalestudents();
      $new['teacher']=$this->pro_model->teachingstaff();
      $new['n_users']=$this->pro_model->newuser();

    	$this->load->view("newusers",$new);
    }
    //registered male students
    function regmale(){

      //male students users table data
      $male['pending']=$this->pro_model->statnewusers();
      $male['male']=$this->pro_model->malestudents();
      $male['female']=$this->pro_model->femalestudents();
      $male['teacher']=$this->pro_model->teachingstaff();
      $male['n_users']=$this->pro_model->newuser();
      $male['m_users']=$this->pro_model->malestude();

    	$this->load->view("male_students",$male);
    }
    //registered female students
    function regfemale(){

      //femlale students users table data
      $fem['pending']=$this->pro_model->statnewusers();
      $fem['male']=$this->pro_model->malestudents();
      $fem['female']=$this->pro_model->femalestudents();
      $fem['teacher']=$this->pro_model->teachingstaff();
      $fem['n_users']=$this->pro_model->newuser();
      $fem['f_users']=$this->pro_model->femalestude();

    	$this->load->view("female_students",$fem);
    }
    //registered teachers
    function regteachers(){

      //teacher users table data
      $tich['pending']=$this->pro_model->statnewusers();
      $tich['male']=$this->pro_model->malestudents();
      $tich['female']=$this->pro_model->femalestudents();
      $tich['teacher']=$this->pro_model->teachingstaff();
      $tich['n_users']=$this->pro_model->newuser();
      $tich['ticha']=$this->pro_model->teachers();

    	$this->load->view("teachers",$tich);
    }
     //loads the register page
    function addnewuser(){

      $this->load->view("register_user");
    }

     //blocked users
    function blocked(){

      //blocked users table data
      $bloc['pending']=$this->pro_model->statnewusers();
      $bloc['male']=$this->pro_model->malestudents();
      $bloc['female']=$this->pro_model->femalestudents();
      $bloc['teacher']=$this->pro_model->teachingstaff();
      $bloc['n_users']=$this->pro_model->newuser();
      $bloc['bloc']=$this->pro_model->blocked();

      $this->load->view("blockedusers",$bloc);
    }

    

    

    

    //loads the calendar page
    function calendar(){
      $this->load->view("calendar");
    }

    //loads the statistical charts page
    function stats(){
      $this->load->view("morris");
    }

    //register new user
    function add_user(){
         $valid=array(
        array(
          'field'=>'fname',
          'label'=>'first name',
          'rules'=>'trim|required|xss_clean|min_length[3]'
          ),
        array(
          'field'=>'lname',
          'label'=>'last name',
          'rules'=>'trim|required|xss_clean|min_length[3]'
          ),
        array(
          'field'=>'reg-no',
          'label'=>'reg/adm no.',
          'rules'=>'trim|required|xss_clean'
          )
        );
      $this->form_validation->set_rules($valid);

      if($this->form_validation->run() === FALSE)
      {
        $this->load->view("register_user"); //redirects back to show errors
      }

      else
      {
        $this->pro_model->adduser();

      
      }
      
    }


  

    //function to reject invalid registered user
     function rejectuser(){
       $this->pro_model->rejectuser($_REQUEST['id']);
        
        $this->session->set_flashdata('rejmsg', 'The user application Rejected');
         redirect('dashboard/newusers');
   }
    //function to confirm a new user
     function confirmuser(){
       $this->pro_model->confirmuser($_REQUEST['id']);
        
        $this->session->set_flashdata('confmsg', 'Successfully Confirmed!');
         redirect('dashboard/newusers');
   }
    //function to block user
     function blockuser(){
       $this->pro_model->blockuser($_REQUEST['id']);
        
        $this->session->set_flashdata('blckmsg', 'One user blocked!');
         redirect('dashboard/blocked');
   }
    //function to delete a user
     function deleteuser(){
       $this->pro_model->deleteuser($_REQUEST['id']);
        
        $this->session->set_flashdata('delmsg', 'One user Deleted!');
         redirect('dashboard');
   }

     //function to unblock user
     function unblockuser(){
       $this->pro_model->unblockuser($_REQUEST['id']);
        
        $this->session->set_flashdata('unblckmsg', 'One user Unblocked!');
         redirect('dashboard/blocked');
   }
   //function view particular user
     function viewuser(){
       $this->load->view('viewusers');
        
      
   }
}