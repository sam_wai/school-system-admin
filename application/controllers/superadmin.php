<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Superadmin extends CI_Controller{

  function __construct(){
   parent::__construct();
   
   
    $this->load->helpers('my_helper');//function to avoid back button to dashboard after login out
    no_cache();

    //side menu data
    $side['blc']=$this->pro_model->blockedusers();
    $side['new']=$this->pro_model->statnewusers();
    $side['mle']=$this->pro_model->malestudents();
    $side['fmle']=$this->pro_model->femalestudents();
    $side['tch']=$this->pro_model->teachingstaff();
    
    $this->load->view("admin_headertop",$side);

   }
  //system admin homepage
  public function index(){
    //stat boxes data
    $stats['pending']=$this->pro_model->statnewusers();
    $stats['male']=$this->pro_model->malestudents();
    $stats['female']=$this->pro_model->femalestudents();
    $stats['teacher']=$this->pro_model->teachingstaff();

    $this->load->view("super_admin_home",$stats);
  }
 //registered schools
    function registeredschools(){

      //blocked users table data
      $regsch['pending']=$this->pro_model->statnewusers();
      $regsch['male']=$this->pro_model->malestudents();
      $regsch['female']=$this->pro_model->femalestudents();
      $regsch['teacher']=$this->pro_model->teachingstaff();
      $regsch['n_users']=$this->pro_model->newuser();

      $regsch['rschs']=$this->pro_model->regschs();

      $this->load->view("registeredschools",$regsch);
    }

     //school admins
    function schooladmns(){

      //blocked users table data
      $admns['pending']=$this->pro_model->statnewusers();
      $admns['male']=$this->pro_model->malestudents();
      $admns['female']=$this->pro_model->femalestudents();
      $admns['teacher']=$this->pro_model->teachingstaff();
      $admns['n_users']=$this->pro_model->newuser();

      $admns['schadmns']=$this->pro_model->schadmns();

      $this->load->view("schooladmins",$admns);
    }
 //register new school
    function regnewschool(){
      $county['cnty']=$this->pro_model->counties();
      $this->load->view("register_school",$county);
     
    }
 //register new school admin
    function add_school_admin(){
      $sch['schs']=$this->pro_model->dropdwnschs();

      
      $this->load->view("register_school_admin",$sch);
     
    }
   //register a new school
    function add_school(){
         $valid=array(
        array(
          'field'=>'sch_name',
          'label'=>'school name',
          'rules'=>'trim|required|xss_clean|min_length[3]'
          ),
        array(
          'field'=>'county',
          'label'=>'location',
          'rules'=>'trim|required|xss_clean|min_length[3]'
          )
        );
      $this->form_validation->set_rules($valid);

      if ($this->form_validation->run() == FALSE)
      {
        $this->load->view('register_school'); //redirects back to show errors
      }

      else
      {
        $this->pro_model->addschool(); 

         if($this->pro_model->addschool()===True)
         {
           $this->session->set_flashdata('succmsg', 'Added successfully!');     
           redirect('superadmin/regnewschool');
         }
      }
      
    }

      //register a new school admin
    function add_admin(){
         $valid=array(
        array(
          'field'=>'username',
          'label'=>'username',
          'rules'=>'trim|required|xss_clean|min_length[3]'
          ),
        array(
          'field'=>'fname',
          'label'=>'firstname',
          'rules'=>'trim|required|xss_clean|min_length[3]'
          ),
        array(
          'field'=>'lname',
          'label'=>'lastname',
          'rules'=>'trim|required|xss_clean|min_length[3]'
          ),
        array(
          'field'=>'email',
          'label'=>'email address',
          'rules'=>'trim|required|xss_clean|min_length[5]|email'
          ),
        array(
          'field'=>'password',
          'label'=>'password',
          'rules'=>'trim|required|xss_clean|min_length[5]'
          )
        );
      $this->form_validation->set_rules($valid);

      if($this->form_validation->run() === FALSE)
      {
        $this->load->view("register_school_admin"); //redirects back to show errors
      }

      else
      {
        $this->pro_model->addschooladmin(); 
      }
      
    }

    //view a school
     function viewsch(){
       $this->pro_model->viewsch($_REQUEST['sch']);

       $data['schdet']=$this->pro_model->viewsch();
       $data['cnty']=$this->pro_model->counties();

       $this->load->view('viewschool',$data);
   }
   function updateschdetails(){
     $this->pro_model->updatesch();
     
   }
   function deletesch(){
    $this->pro_model->deletesch($_REQUEST['sch']);

      $this->session->set_flashdata('delmsg', 'One School Deleted!');
    redirect('superadmin/registeredschools');
   }

   //update school admin
     function editschadmn(){
       $this->pro_model->viewschadmn($_REQUEST['admn']);
       
       $data['admns']=$this->pro_model->viewschadmn();
       $this->load->view('viewschooladmns',$data);
    }
    function update_sch_admin(){
      $this->pro_model->updateschadmn();
    }

    function deleteschadmn(){
      $this->pro_model->deleteschadmn($_REQUEST['admn']);

      $this->session->set_flashdata('delmsg', 'One School Admin Deleted!');
    redirect('superadmin/schooladmns');
    }
}