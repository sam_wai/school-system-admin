<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pro_model extends CI_Model{

  //school admin login function
  function admin_login(){ 

     $this->db->where('username',$this->input->post('username'));
     $this->db->where('password',$this->input->post('password'));

     $q=$this->db->get('sys_admin');
      
      if($q->num_rows()==1)
      {  

            $this->pro_model->getschool(); //calls the function to get the school by name
          return true;
        }
        else 
           return false;


  }
  //system admin login function
  function systemadmin_login(){
     $this->db->where('username',$this->input->post('username'));
     $this->db->where('password',$this->input->post('password'));

     $q=$this->db->get('super_admin');
      
      if($q->num_rows()==1)
      {  

            $company="CarrelTech Ltd";
               //create the session
            $data=array('username'=>$this->input->post('username'),'comp'=>$company,'logged_in'=>true);
            $this->session->set_userdata($data);

          return true;
        }
        else 
           return false;
  }


  //get the school for the logged in admin
  function getschool(){
    $query=$this->db->select('school_name')
                ->from('schools')
                ->join('sys_admin','schools.school_code=sys_admin.school_code','inner')
                ->where('username',$this->input->post('username'))
                ->where('password',$this->input->post('password'))
                ->get();
    if($query->num_rows()==1)
      {  
           $admsch=$query->row()->school_name;
           //create session
           $data=array('username'=>$this->input->post('username'),'school'=>$admsch,'logged_in'=>true);
           $this->session->set_userdata($data);


      }

    
  }
  //register user from admin panel
  function adduser(){
    $this->db->where('regno',$this->input->post('reg-no'));//check if user already exists

     $q=$this->db->get('users');
      
        if($q->num_rows()==1)
        {  

           $this->session->set_flashdata('errmsg', 'User with the same Reg/Adm no exist.Kindly confirm!');
           redirect('dashboard/addnewuser');

  
        }
        else 
        { 
          $this->pro_model->addusertodb();//call the function to add to database
          $this->session->set_flashdata('succmsg', 'User added successfully!');
          
       redirect('dashboard/addnewuser');
      }
      
  }
  //register school from admin panel
  function addschool(){
    $this->db->where('school_name',$this->input->post('sch_name'));//check if school already exists

     $q=$this->db->get('schools');
      
        if($q->num_rows()==1)
        {  

           $this->session->set_flashdata('errmsg', 'School with the same name exist.Kindly confirm!');
           redirect('superadmin/regnewschool');

  
        }
        else 
        { 
          $this->pro_model->addschtodb();//call the function to add to database
          $this->session->set_flashdata('succmsg', 'School added successfully!');
          
       redirect('superadmin/regnewschool');
      }
      
  }

  //register school admin
  function addschooladmin(){
     $this->db->where('school_code',$this->input->post('sch_name'));//check if school already exists

     $q=$this->db->get('sys_admin');
      
        if($q->num_rows()==1)
        {  

           $this->session->set_flashdata('errmsg', 'Admin for that school already exists!Kindly confirm!');
           redirect('superadmin/add_school_admin');

  
        }
        else 
        { 
          $this->pro_model->addadmtodb();//call the function to add to database
          $this->session->set_flashdata('succmsg', 'Added successfully!');
          
       redirect('superadmin/add_school_admin');
      }
      
           
  }
  function addusertodb(){ //function to add user to database
          $this->db->where('username',$this->session->userdata('username'));
          $q=$this->db->get('sys_admin');

          if($q->num_rows()>0){

            $code=$q->row()->school_code;  //fetch school code
           }
           $data=array(
              'fname'=>$this->input->post('fname'),
              'lname'=>$this->input->post('lname'),
              'school_code'=>$code,
              'regno'=>$this->input->post('reg-no'),
              'gender'=>$this->input->post('gender'),
              'user_type'=>$this->input->post('user_type'),
              'date_joined'=>date('Y-m-d H:i:s',now())
              );

            return $this->db->insert('users',$data);
  }

  function addschtodb(){ //function to add school to database
           $data=array(
              'school_name'=>$this->input->post('sch_name'),
              'location'=>$this->input->post('county'),
              'type'=>$this->input->post('sch_type'),
              'reg_date'=>date('Y-m-d H:i:s',now())
              );

            return $this->db->insert('schools',$data);
  }


  function addadmtodb(){ //function to add admin to database
         $data=array(
              'school_code'=>$this->input->post('sch_name'),
              'fname'=>$this->input->post('fname'),
              'lname'=>$this->input->post('lname'),
              'email'=>$this->input->post('email'),
              'username'=>$this->input->post('username'),
              'password'=>$this->input->post('password')
              );

            return $this->db->insert('sys_admin',$data);
  }

  //function fetch all pending users
  function newuser(){
    return $this->db->get_where('users',array('status'=>'Pending'))->result();
  }
  //function fetch male students
  function malestude(){
    return $this->db->get_where('users',array('gender'=>'Male','user_type'=>'Student','status'=>'Confirmed'))->result();
  }
  //function fetch female students
  function femalestude(){
    return $this->db->get_where('users',array('gender'=>'Female','user_type'=>'Student','status'=>'Confirmed'))->result();
  }
  //function fetch teacher users
  function teachers(){
    return $this->db->get_where('users',array('user_type'=>'Teacher','status'=>'Confirmed'))->result();
  }
  //function fetch blocked users
  function blocked(){
    return $this->db->get_where('users',array('status'=>'Blocked'))->result();
  }
  //function fetch registered schools
  function regschs(){
   
    $query=$this->db->select('*')
                ->from('schools')
                ->join('sys_admin','schools.school_code=sys_admin.school_code','left')
                ->get();
     return $query->result();
  }

  //function fetch registered schools
  function schadmns(){  
    $query=$this->db->select('*')
                ->from('schools')
                ->join('sys_admin','schools.school_code=sys_admin.school_code','inner')
                ->get();
     return $query->result();
  }

   //function fetch registered schools for register admin drop down
  function dropdwnschs(){
     return $this->db
                     ->get('schools')
                     ->result();
    }

    //function fetch the Kenyan counties
  function counties(){
     return $this->db->select('county')
                     ->from('counties')
                     ->get()
                     ->result();
    }
   

  /*users summary stat area boxes on dashboard*/
  function statnewusers(){//number of unconfirmed users
    $this->db->where('status', 'Pending');
    $this->db->from('users');

    return $this->db->count_all_results();
  }
  function malestudents(){//number of male students users
    $this->db->where('gender', 'Male');
    $this->db->where('user_type', 'Student');
    $this->db->where('status', 'Confirmed');
    $this->db->from('users');

    return $this->db->count_all_results();
  }
  function femalestudents(){//number of female students users
    $this->db->where('gender', 'Female');
    $this->db->where('user_type', 'Student');
    $this->db->where('status', 'Confirmed');
    $this->db->from('users');

    return $this->db->count_all_results();
  }
  function teachingstaff(){//number of teaching staff users
    $this->db->where('user_type', 'Teacher');
    $this->db->where('status', 'Confirmed');
    $this->db->from('users');

    return $this->db->count_all_results();
  }
  function blockedusers(){//number of blocked users
    $this->db->where('status', 'Blocked');
    $this->db->from('users');

    return $this->db->count_all_results();
  }

  //Rejectected user by admin
  function rejectuser(){
    return $this->db->delete('users',array('user_id'=>$_REQUEST['id']));
  }
   //Confirm user by admin
  function confirmuser(){
    $data=array('status'=>'Confirmed');

    $this->db->where('user_id',$_REQUEST['id']);
    $this->db->update('users',$data);

  }
   //Block user
  function blockuser(){
    $data=array('status'=>'Blocked');

    $this->db->where('user_id',$_REQUEST['id']);
    $this->db->update('users',$data);

  }
  //delete user
  function deleteuser(){
    return $this->db->delete('users',array('user_id'=>$_REQUEST['id']));
  }
   //unblock user
  function unblockuser(){
    $data=array('status'=>'Confirmed');

    $this->db->where('user_id',$_REQUEST['id']);
    $this->db->update('users',$data);

  }

   //view a school
  function viewsch(){
    $query=$this->db->select('*')
                ->from('schools')
                ->join('sys_admin','schools.school_code=sys_admin.school_code','left')
                ->where('school_name',$_REQUEST['sch'])
                ->get();

     return $query->result();
  }
  function updatesch(){
    $data=array('school_name'=>$this->input->post('sch_name'),
                'location'=>$this->input->post('county'),
                'type'=>$this->input->post('sch_type'));

    $this->db->where('school_code',$this->input->post('sch_id'));
    $this->db->update('schools',$data);

      $this->session->set_flashdata('succmsg', 'Successfully Updated!');     
     redirect('superadmin/registeredschools');
     
  }
  function deletesch(){
    return $this->db->delete('schools',array('school_code'=>$_REQUEST['sch']));
      
  }

  //view school admin 
  function viewschadmn(){
    $query=$this->db->select('*')
                ->from('schools')
                ->join('sys_admin','schools.school_code=sys_admin.school_code','left')
                ->where('school_name',$_REQUEST['admn'])
                ->get();

     return $query->result();
  }
   function updateschadmn(){
    $data=array('fname'=>$this->input->post('fname'),
                'lname'=>$this->input->post('lname'),
                'email'=>$this->input->post('email'),
                'username'=>$this->input->post('username'),
                'password'=>$this->input->post('password'));

    $this->db->where('admin_id',$this->input->post('admin_id'));
    $this->db->update('sys_admin',$data);

      $this->session->set_flashdata('succmsg', 'Successfully Updated!');     
     redirect('superadmin/schooladmns');
     
  }
  function deleteschadmn(){
    return $this->db->delete('sys_admin',array('admin_id'=>$_REQUEST['admn']));
      
  }
}