<?php
  if(($this->session->userdata('username')==""))
     {    
        redirect('dashboard');                       
     } 
?>

            <!-- Right side column. Contains the navbar and content of the page -->
          <aside class="right-side">
          <!-- Content Header (Page header) -->
             <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Register New User</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">Add New User</li>
                    </ol>
                </section>
            <!-- Main content -->
                <section class="content">
                <?php echo validation_errors();?>
                <?php echo form_open('dashboard/add_user');?>
                 <!--the error message-->
                    <?php if($this->session->flashdata('errmsg')): ?>
                        <div class="alert alert-danger" style="text-align:center">
                            <a href="" class="close" data-dismiss="alert">&times;</a>   
                              <p><?php echo $this->session->flashdata('errmsg');?></p>
                        </div>
                    <?php endif; ?>
                    <!--the success message-->
                    <?php if($this->session->flashdata('succmsg')): ?>
                        <div class="alert alert-success" style="text-align:center">
                            <a href="" class="close" data-dismiss="alert">&times;</a>   
                              <p><?php echo $this->session->flashdata('succmsg');?></p>
                        </div>
                    <?php endif; ?>
                  <form role="form">
                        <div class="box-body">
                            <div class="form-group regform">
                                <label for="First Name">First Name</label>
                                <input type="text" class="form-control" name="fname" placeholder="first name" required="required">
                            </div>
                            <div class="form-group regform">
                                <label for="LastName">Last Name</label>
                                <input type="text" class="form-control" name="lname" placeholder="last name" required="required">
                            </div>
                            
                            <div class="form-group regform">
                                <label for="type">Type</label>
                                <select name="user_type" class="form-control">
                                    <option>Student</option>
                                    <option>Teacher</option>
                                </select>
                            </div>
                            <div class="form-group regform">
                                <label for="type">Gender</label>
                                <select name="gender" class="form-control">
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>
                            </div>
                            <div class="form-group regform">
                                <label for="reg no">Adm/ID No.</label>
                                <input type="text" class="form-control" name="reg-no" placeholder="adm no/id no" required="required">
                            </div>
                    
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary regformbutton">Submit</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </form>
                </section>
              
          </aside>
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->

        
        <!-- DATA TABES SCRIPT -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js" type="text/javascript"></script>

    </body>
    
    </body>
</html>
