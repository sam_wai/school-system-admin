<?php
  if(($this->session->userdata('username')==""))
     {    
        redirect('login');                       
     } 
?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>View School Details</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="superadmin"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="superadmin">Dashboard</a></li>
                        <li class="active">School Details</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                <h1>Details:</h1>
               <?php
                 foreach ($schdet as $key) {
                 	?>
                 	<table width="330px" style="font-size:15px">
	                 	<tr><td><?php echo "<b>School Name:</b> ";?></td><td><?php echo $key->school_name;?></td></tr>
	                 	<tr><td><?php echo "<b>School Location:</b> ";?></td><td><?php echo $key->location." County";?></td></tr>
	                 	<tr><td><?php echo "<b>School Type:</b> ";?></td><td><?php echo $key->type." School";?></td></tr>
	                 	<tr><td><?php echo "<b>School Join Date:</b> ";?></td><td><?php echo $key->reg_date;?></td></tr>
	                 	<tr><td><?php echo "<b>School Admin:</b> ";?></td><td><?php if($key->username=="")
	                 	                                                                 {
	                 	                                                                 	echo anchor('superadmin/add_school_admin',"Add school admin");
	                 	                                                                 } 
	                 	                                                            else 
	                 	                                                            	echo $key->username;
	                 	                                                            ?></td></tr>
	                 
                    </table>
                  <h1>Update:</h1>
                  	<?php echo validation_errors();?>
	                <?php echo form_open('superadmin/updateschdetails');?>
	                        
                      <form role="form">
                        <div class="box-body">
                            <input type="hidden" value="<?php echo $key->school_code;?>" name="sch_id">
                            <div class="form-group regform">
                                <label for="LastName">School Name</label>
                                <input type="text" class="form-control" name="sch_name" value="<?php echo $key->school_name;?>" required="required">
                            </div>

                            <div class="form-group regform">
                                <label for="LastName">School Location</label>
                                <select name="county" class="form-control">
                                   <?php foreach ($cnty as $cnt) {
                                    ?>
                                    <option value="<?php echo $cnt->county;?>"><?php echo $cnt->county;?></option>
                                    <?php
                                     }
                                    ?>
                                </select>
                            </div>
                            
                            <div class="form-group regform">
                                <label for="type">Type</label>
                                <select name="sch_type" class="form-control">
                                    <option>National</option>
                                    <option>County</option>
                                    <option>District</option>
                                    <option>Private</option>
                                </select>
                            </div>
                            
                    
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary regformbutton">Update</button>
                         
                            <?php echo anchor('superadmin/registeredschools','<span class="btn btn-danger">Cancel</span>');?>
                           
                        </div>
                    </form>
                   <?php
	                 }
	               ?>

     

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->

        
        <!-- DATA TABES SCRIPT -->
       <?php include 'application/includes/bottom_includes.php';?>

    </body>
    
    </body>
</html>
