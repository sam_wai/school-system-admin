<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Admin | Log in</title>
        <?php include 'application/includes/top_includes.php';?>
    </head>
    <body class="bg-black">
 
       <!--login form-->
        <div class="form-box" id="login-box">
           <div class="header">System Admin Log In</div>
            <?php echo validation_errors();?>
            <?php echo form_open('dashboard/login');?>
                <form role="form" method="post">
                   
                    <div class="body bg-gray">
                    <!--the login fail error-->
                    <?php if($this->session->flashdata('msg')): ?>
                        <div class="alert alert-danger">
                            <a href="" class="close" data-dismiss="alert">&times;</a>   
                              <p><?php echo $this->session->flashdata('msg');?></p>
                        </div>
                    <?php endif; ?>
                    
                    <!--the login fail error end-->
                        <div class="form-group">
                            <select class="form-control" name="type">
                                <option>School Admin</option>
                                <option>System Admin</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" name="username" class="form-control" placeholder="Username"required="required"/>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Password" required="required"/>
                        </div>
                        <!-- <div class="form-group">
                            <input type="checkbox" name="remember_me"/> Remember me
                        </div> -->
                    </div>
                    <div class="footer">
                        <button type="submit" class="btn bg-olive btn-block">Sign me in</button>

                        <p><a href="#">I forgot my password</a></p>
                    </div>
                </form>

                <div class="margin text-center">
                    <span>Copyrights Reserved Carreltech @ 2015</span>
                </div>
        </div>
       <?php include 'application/includes/bottom_includes.php';?>

    </body>
</html>
