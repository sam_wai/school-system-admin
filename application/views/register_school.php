<?php
  if(($this->session->userdata('username')==""))
     {    
        redirect('login');                       
     } 
?>

            <!-- Right side column. Contains the navbar and content of the page -->
          <aside class="right-side">
          <!-- Content Header (Page header) -->
             <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Register School</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="superadmin"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="superadmin">Dashboard</a></li>
                        <li class="active">Add School</li>
                    </ol>
                </section>
            <!-- Main content -->
                <section class="content">
                <?php echo validation_errors();?>
                <?php echo form_open('superadmin/add_school');?>
                 <!--the error message-->
                    <?php if($this->session->flashdata('errmsg')): ?>
                        <div class="alert alert-danger" style="text-align:center">
                            <a href="" class="close" data-dismiss="alert">&times;</a>   
                              <p><?php echo $this->session->flashdata('errmsg');?></p>
                        </div>
                    <?php endif; ?>
                    <!--the success message-->
                    <?php if($this->session->flashdata('succmsg')): ?>
                        <div class="alert alert-success" style="text-align:center">
                            <a href="" class="close" data-dismiss="alert">&times;</a>   
                              <p><?php echo $this->session->flashdata('succmsg');?></p>
                        </div>
                    <?php endif; ?>
                  <form role="form">
                        <div class="box-body">
        
                            <div class="form-group regform">
                                <label for="LastName">School Name</label>
                                <input type="text" class="form-control" name="sch_name" placeholder="name" required="required">
                            </div>
                            
                            <div class="form-group regform">
                            <label for="county">County</label>
                            <select name="county" class="form-control">
                                   <?php foreach ($cnty as $cnt) {
                                    ?>
                                    <option value="<?php echo $cnt->county;?>"><?php echo $cnt->county;?></option>
                                    <?php
                                     }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group regform">
                                <label for="type">Type</label>
                                <select name="sch_type" class="form-control">
                                    <option>National</option>
                                    <option>County</option>
                                    <option>District</option>
                                    <option>Private</option>
                                </select>
                            </div>
                            
                    
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary regformbutton">Submit</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </form>
                </section>
              
          </aside>
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->
        <?php include 'application/includes/bottom_includes.php';?>

    </body>
    
    </body>
</html>