<?php
  if(($this->session->userdata('username')==""))
     {    
        redirect('login');                       
     } 
?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>School Admin</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="superadmin"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="superadmin">Dashboard</a></li>
                        <li class="active">School Admin Details</li>
                    </ol>

                </section>

                <!-- Main content -->
                <section class="content">
                
                  <?php
                     foreach ($admns as $key) {
                         ?>
                         <h2 class="regform"><?php echo "School admin : ".$key->school_name;?></h2><br>

                         <!--Update form-->
                <?php echo validation_errors();?>
                <?php echo form_open('superadmin/update_sch_admin');?>
                  <form role="form">
                        <div class="box-body">
                            <input type="hidden" value="<?php echo $key->admin_id;?>" name="admin_id">
                            <div class="form-group regform">
                                <label for="LastName">First Name</label>
                                <input type="text" class="form-control" name="fname" value="<?php echo $key->fname;?>" required="required">
                            </div>

                            <div class="form-group regform">
                                <label for="LastName">Last Name</label>
                                <input type="text" class="form-control" name="lname" value="<?php echo $key->lname;?>" required="required">
                            </div>

                            <div class="form-group regform">
                                <label for="LastName">Email Address</label>
                                <input type="text" class="form-control" name="email" value="<?php echo $key->email;?>" required="required">
                            </div>

                            <div class="form-group regform">
                                <label for="LastName">Username</label>
                                <input type="text" class="form-control" name="username" value="<?php echo $key->username;?>" required="required">
                            </div>
                            
                            <div class="form-group regform">
                                <label for="reg no">Password</label>
                                <input type="password" class="form-control" name="password" value="<?php echo $key->password;?>" required="required">
                            </div>

                            
                    
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary regformbutton">Update</button>
                            <?php echo anchor('superadmin/schooladmns','<span class="btn btn-danger">Cancel</span>');?>
                        </div>
                    </form>
                    <?php
                     }
                    ?>
     

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->

        
        <!-- DATA TABES SCRIPT -->
       <?php include 'application/includes/bottom_includes.php';?>

    </body>
    
    </body>
</html>
