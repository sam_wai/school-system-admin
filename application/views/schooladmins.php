<?php
  if(($this->session->userdata('username')==""))
     {    
        redirect('login');                       
     } 
?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>School Admins</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="superadmin"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="superadmin">Dashboard</a></li>
                        <li class="active">School Admins</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                      <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
                                      <?php 
                                        print_r($pending);
                                       ?>
                                    </h3>
                                    <p>
                                        New Users
                                    </p>
                                </div>
                                <div class="icon">
                                    
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="newusers" class=" small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        <?php 
                                        print_r($male);
                                       ?>
                                    </h3>
                                    <p>
                                        Male Students
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-male"></i>
                                </div>
                                <a href="regmale" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                        <?php 
                                        print_r($female);
                                       ?>
                                    </h3>
                                    <p>
                                        Female Students
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-female"></i>
                                </div>
                                <a href="regfemale" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>
                                        <?php 
                                        print_r($teacher);
                                       ?>
                                    </h3>
                                    <p>
                                        Teachers
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-stalker"></i>
                                </div>
                                <a href="regteachers" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                    </div><!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
                      <div class="col-xs-12">
                         <div class="box">
                                <div class="box-header">
                                <!--the delete message-->
                                <?php if($this->session->flashdata('delmsg')): ?>
                                    <div class="alert alert-danger" style="text-align:center">
                                        <a href="" class="close" data-dismiss="alert">&times;</a>   
                                          <p><i class="fa fa-trash"></i>&nbsp;&nbsp;<?php echo $this->session->flashdata('delmsg');?></p>
                                    </div>
                                <?php endif; ?>
                                <!--the success message-->
                                <?php if($this->session->flashdata('succmsg')): ?>
                                    <div class="alert alert-success" style="text-align:center">
                                        <a href="" class="close" data-dismiss="alert">&times;</a>   
                                          <p><?php echo $this->session->flashdata('succmsg');?></p>
                                    </div>
                                <?php endif; ?>
                              <h3 class="box-title"><i class="fa fa-user">&nbsp;&nbsp;</i>School Admins</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Username</th>
                                                <th>School</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                         
                                            <?php foreach ($schadmns as $adms) {
                                             echo '<tr><td>'.$adms->fname.'</td>
                                                      <td>'.$adms->lname.'</td>
                                                      <td>'.$adms->username.'</td>
                                                      <td>'.$adms->school_name.'</td>
                                                      <td>'.anchor('superadmin/editschadmn?admn='.$adms->school_name,'<i class="fa fa-pencil"style="float;left"></i>').anchor('superadmin/deleteschadmn?admn='.$adms->admin_id,'<i class="fa fa-trash-o" style="float:right"></i>').'</td>
                                                  </tr>';
                                            }?>
                                        
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Username</th>
                                                <th>School</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                      </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->

        
        <!-- DATA TABES SCRIPT -->
       <?php include 'application/includes/bottom_includes.php';?>

        <script src="<?php echo base_url();?>application/assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>application/assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url();?>application/assets/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url();?>application/assets/js/AdminLTE/demo.js" type="text/javascript"></script>
        

        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>

    </body>
    
    </body>
</html>
