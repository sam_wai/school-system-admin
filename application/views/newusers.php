<?php
  if(($this->session->userdata('username')==""))
     {    
        redirect('dashboard');                       
     } 
?>
            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Dashboard
                        <small>New users</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="dashboard">Dashboard</a></li>
                        <li class="active">New users</li>
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">

                      <!-- Small boxes (Stat box) -->
                    <div class="row">
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <h3>
                                      <?php 
                                        print_r($pending);
                                       ?>
                                    </h3>
                                    <p>
                                        New Users
                                    </p>
                                </div>
                                <div class="icon">
                                    
                                    <i class="ion ion-person-add"></i>
                                </div>
                                <a href="newusers" class=" small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-green">
                                <div class="inner">
                                    <h3>
                                        <?php 
                                        print_r($male);
                                       ?>
                                    </h3>
                                    <p>
                                        Male Students
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-male"></i>
                                </div>
                                <a href="regmale" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-yellow">
                                <div class="inner">
                                    <h3>
                                        <?php 
                                        print_r($female);
                                       ?>
                                    </h3>
                                    <p>
                                        Female Students
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-female"></i>
                                </div>
                                <a href="regfemale" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                        <div class="col-lg-3 col-xs-6">
                            <!-- small box -->
                            <div class="small-box bg-red">
                                <div class="inner">
                                    <h3>
                                        <?php 
                                        print_r($teacher);
                                       ?>
                                    </h3>
                                    <p>
                                        Teachers
                                    </p>
                                </div>
                                <div class="icon">
                                    <i class="ion ion-person-stalker"></i>
                                </div>
                                <a href="regteachers" class="small-box-footer">
                                    More info <i class="fa fa-arrow-circle-right"></i>
                                </a>
                            </div>
                        </div><!-- ./col -->
                    </div><!-- /.row -->

                    <!-- Main row -->
                    <div class="row">
                      <div class="col-xs-12">
                         <div class="box">
                                <div class="box-header">
                                <!--the reject message-->
                                <?php if($this->session->flashdata('rejmsg')): ?>
                                    <div class="alert alert-danger" style="text-align:center">
                                        <a href="" class="close" data-dismiss="alert">&times;</a>   
                                          <p><i class="fa fa-warning"></i>&nbsp;&nbsp;<?php echo $this->session->flashdata('rejmsg');?></p>
                                    </div>
                                <?php endif; ?>

                                <!--the confirm message-->
                                <?php if($this->session->flashdata('confmsg')): ?>
                                    <div class="alert alert-success" style="text-align:center">
                                        <a href="" class="close" data-dismiss="alert">&times;</a>   
                                          <p><i class="fa fa-check-circle"></i>&nbsp;&nbsp;<?php echo $this->session->flashdata('confmsg');?></p>
                                    </div>
                                <?php endif; ?>
                                    <h3 class="box-title"><i class="fa fa-user"></i>&nbsp;&nbsp;New Users Waiting For Confirmation!</h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive">
                                    <table id="example1" class="table table-bordered table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Reg/Roll No.</th>
                                                <th>User Type</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                         
                                            <?php foreach ($n_users as $new) {
                                             echo '<tr><td>'.$new->fname.'</td>
                                                      <td>'.$new->lname.'</td>
                                                      <td>'.$new->regno.'</td>
                                                      <td>'.$new->user_type.'</td>
                                                      <td>'.anchor('dashboard/confirmuser?id='.$new->user_id,'<i class="fa fa-check"style="padding-left:20px;"></i>').anchor('dashboard/rejectuser?id='.$new->user_id,'<i class="fa fa-times" style="float:right"></i>').'</td>
                                                  </tr>';
                                            }?>
                                        
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Reg/Roll No.</th>
                                                <th>User Type</th>
                                                <th>Action</th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->
                      </div>
                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->

        
        <!-- DATA TABES SCRIPT -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js" type="text/javascript"></script>

        <script src="<?php echo base_url();?>application/assets/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
        <script src="<?php echo base_url();?>application/assets/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url();?>application/assets/js/AdminLTE/app.js" type="text/javascript"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url();?>application/assets/js/AdminLTE/demo.js" type="text/javascript"></script>
        

        <script type="text/javascript">
            $(function() {
                $("#example1").dataTable();
                $('#example2').dataTable({
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": false,
                    "bSort": true,
                    "bInfo": true,
                    "bAutoWidth": false
                });
            });
        </script>

    </body>
    
    </body>
</html>
