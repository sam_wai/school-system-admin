<?php
  if(($this->session->userdata('username')==""))
     {    
        redirect('login');                       
     } 
?>

            <!-- Right side column. Contains the navbar and content of the page -->
          <aside class="right-side">
          <!-- Content Header (Page header) -->
             <section class="content-header">
                    <h1>
                        Dashboard
                        <small>Register School Admin</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="superadmin"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li><a href="superadmin">Dashboard</a></li>
                        <li class="active">Register Admin</li>
                    </ol>
                </section>
            <!-- Main content -->
                <section class="content">
                <?php echo validation_errors();?>
                <?php echo form_open('superadmin/add_admin');?>
                 <!--the error message-->
                    <?php if($this->session->flashdata('errmsg')): ?>
                        <div class="alert alert-danger" style="text-align:center">
                            <a href="" class="close" data-dismiss="alert">&times;</a>   
                              <p><?php echo $this->session->flashdata('errmsg');?></p>
                        </div>
                    <?php endif; ?>
                    <!--the success message-->
                    <?php if($this->session->flashdata('succmsg')): ?>
                        <div class="alert alert-success" style="text-align:center">
                            <a href="" class="close" data-dismiss="alert">&times;</a>   
                              <p><?php echo $this->session->flashdata('succmsg');?></p>
                        </div>
                    <?php endif; ?>
                  <form role="form">
                        <div class="box-body">

                            <div class="form-group regform">
                                <label for="type">School Name</label>
                                <select name="sch_name" class="form-control">
                                   <?php foreach ($schs as $sc) {
                                    ?>
                                    <option value="<?php echo $sc->school_code;?>" readonly><?php echo $sc->school_name;?></option>
                                    <?php
                                     }
                                    ?>
                                </select>
                            </div>
                            
                            <div class="form-group regform">
                                <label for="LastName">First Name</label>
                                <input type="text" class="form-control" name="fname" placeholder="first name" required="required">
                            </div>

                            <div class="form-group regform">
                                <label for="LastName">Last Name</label>
                                <input type="text" class="form-control" name="lname" placeholder="last name" required="required">
                            </div>

                            <div class="form-group regform">
                                <label for="LastName">Email Address</label>
                                <input type="text" class="form-control" name="email" placeholder="email" required="required">
                            </div>

                            <div class="form-group regform">
                                <label for="LastName">Username</label>
                                <input type="text" class="form-control" name="username" placeholder="username" required="required">
                            </div>
                            
                            <div class="form-group regform">
                                <label for="reg no">Password</label>
                                <input type="password" class="form-control" name="password" placeholder="password" required="required">
                            </div>

                            
                    
                        </div>

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary regformbutton">Submit</button>
                            <button type="reset" class="btn btn-danger">Cancel</button>
                        </div>
                    </form>
                </section>
              
          </aside>
        </div><!-- ./wrapper -->

        <!-- add new calendar event modal -->

        
        <?php include 'application/includes/bottom_includes.php';?>

    </body>
    
    </body>
</html>